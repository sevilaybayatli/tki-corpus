IRAK TÜRKMEN FOLKLORUNDA ATA TERZİBAŞI’NIN YERİ
ARSHED OMEED JIHAD AWJI
2018
https://tez.yok.gov.tr/UlusalTezMerkezi/tezSorguSonucYeni.jsp

üzerliksen (sedefotu),
havasan yeddi derdedavasan.
Allah'dan istemişem .
Bu derdi sen kovasan.
Üzerliksen, mezesen
Damar-damar göze sen
O güne kurban olum
Evimizde gezesen.

Kurbanıv olum, başıva dönüm ,
Gözüve hayran, boyuva kurban
Allah sağ olasan, dünya durdukça durasan,
Kadav alım, öğüvde ölüm
Sözüvü şekerden kesim
Kan ettim kapıva düştüm
Büyügümsen, ağamsan
Meni eliboş gönderme.

Ağ baht,
Altun taht ,
Kara halayık,
Beyaz ekmekçi,
Siyah köle,
Dilerem kızıma,
Şerri şeytannan,
Kuru buhtannan,
Hayasız arvattan,
At şerrinden,
Od şerrinden,
Az ve asman şerrinden,
Yıkılmış duvardan,
Kudurmuş köpekten,

Kurbanam kaşıva yar
Koy dönüm başıva yar
Meni bu gamdan kurtar
Dolandır başıva yar …. .
Ocağıv;
Daim yansın ocağıv
Evladın kaydına kal
Kör olmasın ocağıv.


-Belsarma olasan (belin kırılsın)
- Kırran kırsın(veba tutsun)
-yaman sancı tutasan
-Gözüve kara su ensin
-Gözüve mil çekilsin
-Kudurasan
-Ölet süpürsün (ve ba götürsün)
-Perkem tutasan (deliresen)
- Yaman yara çıharasan
Bağçaya serdim kilim
Gel eyleş menim gülüm
Ne dedim küstüv gene
Lal olsun menim dilim .
Yaman yara
Yaman derd yaman yara
Yamanlar yahşı oldu
Biz olduk yaman yara
Meni yardan edeni
Çıkarsın yaman yara.

Hey hökülsün hökülsün
Katlı dağlar sökülsün
Arzünu bezedenin
On barmağı tökülsün.
Men kamberem dağ kimin
Titrerem yarpağ kimin
Arzu'nu bezedeni
Kara girsin zağ kimin.

Bele bağlar
Dost başın bele bağlar
Bülbül ağlar, gül açmaz
Virandı bele bağlar
Can çıhma dost gelince
Görek ne deyib ağlar
Kül o adam başına
Bedasıla bel bağlar
Çek ayrılıg derdini
Gör yürek nece dağlar (Paşayev, 1984: 88.)

Uçurdum kara tavug
Kondu erbile yavug
Kamu gedenler döndü
Dönmedi başı savuk (Terzibaşı, 1975: 88).

Allah bir yağız eyle
Dam duvarı yaş eyle
Ağam burdan geçende
Yemenisin yaş eyle
derler.
Evlerden verilen hediyeler geçikirse,
Vereni hatun ossun
Vermeyeni katır ossun
diyerek yüksek sesle bağırırlar bir de:
Dura dura yorulduk
Tütünüvüzden boğulduk
Açın anbar ağzını
Verin Allah borcunu

Çemçele kız:
Çemçele kız aş ister,
Allah'tan yağış ister

Yağış yağar hanavlar,
Felek he miskin avlar,
Feleğin adetidi,
Dayim he miskin avlar
Yağış yağar sesi var,
Kar yağar havası var,
Gideğ felek yanına
Göreğ bizde nesi var
(Benderoğlu Abdullatif, a, g. e. s. 99-101)

1. Bir kızım var bir gözü var(iğne)
2.Gece gündüz seleşi (saat)
3. Baldan şirin baltadan ağır (uyku)
Manzum bilmece:
Gider gider yerinde
Altun kemer belinde
Gece gündüz yol eder
Gene durup yerinde (değirmetaşı)

-Gözden görülü, yohsa görülmez
-Yilini yohsa yilinmez
-Suda yaşa yohsa havada
-Diridi yohsa ölüdü

Sene Bağdadı veremdi sele? Der. Bilmeceyi soranda:
Kendisine Bağdadın verilmesi icap edince bilmeceyi soran bulmayana
Bağdat'ı yiyim
İçiyim
Çenekeve sıçıyım
Tapmacamı nişin tapmadıv
Et götünden kapmadıv
Et yağlıdı
Çenekeve bağlıdı.
Veya:
Men mindim al ata
Sen mindiv kotur begire
Men zıhtaladığca baldan yağ yerem
Senzıhtaladığca kanan irin yesen
Ve:
Men çağırdım at gessin
Sen çağırdıv at gessin
Menim için bir al al geldi
Seniv için bir tazı geldi
Men mindim al ata
sen mindiv tazıya
Çağırdım Bağvancı
Bir teruzü geti.

-Yiyecek ve içeceklerle ilgili bilmeceler:
-Sebzelerle ilgili bilmeceler.
-Meyvelerle ilgili bilmeceler.
-Kuru yemişlerle ilgili bilmeceler.
-Tabiatla ilgili bilmeceler.
-İnsan organlarıyla ilgili bilmeceler.
-Dinle ilgili bilmeceler.
-Eşyalarla ilgili bilmeceler.
-Süs eşyalarıyla ilgili bilmeceler.
-Giyeceklerle ilgili bilmeceler.
-Müzik-çalgı aletleriyle ilgili bilmeceler.
-Hayvanlarla ilgili bilmeceler.
-Ölüm ve ahiret inancıyla ilgili bilmeceler.
-Ağaçlarla ilgili bilmeceler.
-Çeşitli bilmeceler.
Numuneler
-Abdest alı namaz kılmaz
Cemaatten geri kalmaz(ölü)
-Ağ ilan ağzın açar
Qere ilan vurar geçer (iğne-iplik)
-Al geti alvar geti
El degmemiş ağaçtan
Kohlanmamış gül geti (gelin)
-Alaca nimder
Kulpunu dönder(ig)
-Allah yapar yapısını
Demir açar kapısını(karpuz)
-Altı deri üstü deri
İçinde var bir avuç darı (incir) (Asafoğlu, 2010: 70)

Nece dağlar,
Karşıda nece dağlar
Yetim yanağı bilir
Göz yaşı nece dağlar
Durdu gam;
Açdı yaram, durdu gam,
Men miskin olduğumçün
Üstüme kudurdu gam
Kanad ağlar;
Oh titrer, kanda ağlar,
Avam yaralı getdi,
Boyandı kana dağlar
Yaz bele;
Bahar bele, yaz bele
Katibin ne suçu var,
Hudam demiş yaz bele
……………….
Derd meni;
Derd meni, yara meni
Ya al, yaradan, canım
Ya yetir yara meni

Bağçayam, baram sene;
Heyvayam, naram sene
Gevliv bir fikretmesin
Ölğnce yaram sene
…………. .
Yağış yağar göllere,
Bülbül konar güllere
Menim vefasız yarım
Salıb meni çöllere
………….
Mısraları3+1+3ve 3+4 biçiminde olan maniler;
Dıvara daş koymuşam;
Yğrunda baş koymuşam
Gözlerimde kan geli
Adını yaş koymuşam
………. .
Ekinim ekiliri
Sünbülüm tökülürü
Zülfüvden bir tel gönder
Kefinim tikiliri
Mısraları 1+2+2+2ve2+2+1+2 biçiminde olan maniler:
Ay çıhdı bedir Allah,45
Can dost nezir, Allah
Ya canım cana yetir
Ya cana sabır, Allah
…………. .
Kaşıv gözüv zil kimin
Titrer kızıl gül kimin
Kapında abdal oldum
Alıp satma kul kimin

Bağça bar üstünedi
Hayva nar üstünedi
Bu zamanın hoyratı
Hamsı yar üstündei
…………….
Çol yerin cayranıyam
Yar boyuv hayranıyam
Nahsı yoldan gelibsen
O yolun kurbanıyam

Leyle balam leyle
Leyle gözüm leyle
Leyle canım leyle
Leyle şirinim leyle
Ülle balam ülle

Oyağ kallam yatınca
Beklerem ay batınca
Kolum yastığ ederem
Sen hasıla çatınca
Leyla balam leyla
Leyle gülüm Leyla
Leyla diler
Yatıbdı, Leyla diler
Vurgun yuhu salıbdı,
Mennen bir Leyla diler
Ülle balam, ülle,
Ülle ruhum, ülle, (Abdüllatif Benderoğlu, a. g. e. , s. 44)

Layla edim men dayim,
Yuhun olsun mülayim,
Hudama çoh yalvarram
Olasan menim payım
Leyla balam, Leyla,
Leyla ömrüm, Leyla.
………. .
Yağış yağar göl olu,
Kervan geçer, yol olu
Seniv ahırşer nenev
Yatmaz yatmaz, kör olu,
Leyla balam, Leyla
Leyla gözüm, Leyla.
Leyla ederem naçar,
Leyla derd ögün açar
Hudama çoh yalvarram,
Belki derdivden geçer
Leyla balam, Leyla
Leyla ömrüm Leyla

Ataram tutaram seni,
Şekere kataram seni
Akşam babav gelende
Gucuna ataram seni
……………
Kızım kızlar içinde
Altun hiçil gıçında
Kızıma dilekçi geli
Bugün yarın içinde


Günde men;
Kölgede sen, günde men
İlde kurban bir olı
Kurbanıvam günde men
Kurbanam özüm sene,
Heketim, sözüm sene
Işığlı yulduz kimin
Tikmişem gözüm sene

Aman aman içinde
Karbil saman içinde
Deve dallallığ eder
Eski hamam içinde
Hamamçının tası yoh
Peştamalın ortası yoh
Köyümüze bir tazı gelip
Boynunda haltası yoh


Bir kişi hurma satar
Hurmanı geti men yim
Bahah kim borca batar
-Matal matal matına,
şeytan mindi atına,
kamçisini dolantırdı,
dolantırdı
çaldı(…….)başına.
Matalların Sonundaki Bitiş Formülleri
-Havadan üç alma düştü,
biri menim ağzıma,
de(……)ağzına.
-Mende gettim, bir şey vermediler.
Matalların ortasında geçen deyimler:
Az gettiler, uz gettiler, dere tepe düz gettiler.

Keçel oğlanın vasf:
Kaçsa tutulmaz
Oynasa utulmaz
Güleşse yıhılmaz
Günnen evvel aldanı
Gün çıhsa aldanmaz

Sor mes'ele mes'eleye hekimol
Su damlaya olur göl
Ey yari enis çare sazım
Bu hamır çoh su götürür
Bu ölü bu şivene değmez
-Ekmedik bastan, yemedik karpuz
-Evdeki hesap bazarda çıhmaz
-ilde ayda bir namaz, o da hakka yaramaz
-Karnı kuruldar, üstü parıldar
-Kazan yuvarlanır, öz kapağını tapar
-Kızım sene diyirem, gelinim sen eşit
-Eken öküz, biçen , biçen öküz harmana gelene ho….
-Ev buzovu cütde olmaz
-Adı biçindedi, orağı kırıhdı
-Kuş var eti yilini, kuş var et yedirdilir
-Talihsiz uşağın anasını al aparı
Kamber arzular gider
Besleme yad ördeğin
Vatan arzular gider
Ekme bitmeyen yere
Can ver itmeyen yere
Ayaglar nece varsın
Gönül getmeyen yere

Hannan sevgilim, hannan,
Mehebbetir çıhmaz cannan
Dizginini çekerek tut,
Dabanım doldu kannan
Arzu ise :
Hannan kamberim, hannan
Mehebbetiv çıhmaz cannan
Dabanıvnan gelen kan
Gelsin menim gözümnen
Hoyratın söylüyor.

Hak aşığı olduğu için Arzu'nun gözlerinden yaş yerine kan akmış.
Tat, buna kızdı. Kayınbaba bir kamçı aldı eline, verdi Kamberi kamçı önüne. Bele vurdu
bele vurdu, etdi özünü leş.
Döndü kamber yandığından dedi:
Tat oğlu külece boy,
Etdin han Arzuma toy,
Keresin men almışam,

























